var gulp 		= require('gulp'); 				// Main Build System
var sass		= require('gulp-sass'); 		// Builds CSS files from SASS Files
var browserify 	= require('gulp-browserify'); 	// Bundles modules into single Bundles
var browserSync = require('browser-sync'); 		// Browser Sync for live reloading
var concat 		= require('gulp-concat'); 		// Concats multiple files into one file
var hbsfy 		= require('hbsfy'); 			// Compiles Handlebar templates for Browserify
var uglify 		= require('gulp-uglify'); 		// Obfuscate and minify JS files
var sourcemaps	= require('gulp-sourcemaps');	// Add Sourcemaps to outpute
var prefixr		= require('gulp-autoprefixer');	// Autoprefixer for CSS
var sassdoc		= require('sassdoc');			// RELEASE THE DOCS! (TODO: FIX OUTPUT)

// Bundle Modules and Merge them into one minified/obfuscated file
gulp.task('browserify', function() {
	return gulp.src('src/js/main.js')
		.pipe(browserify({transform: 'hbsfy'}))
		.pipe(sourcemaps.init())
		.pipe(concat('main.js'))
		.pipe(uglify())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('dist/js'));
});

// Compiles SASS files into a minified CSS file
gulp.task('sass', function() {
	return gulp.src('src/scss/*.scss')
		.pipe(sourcemaps.init())
		.pipe(sass({
			outputStyle: 'compressed'
		})
		.on('error', sass.logError))
		.pipe(sourcemaps.write())
		.pipe(prefixr())
		.pipe(gulp.dest('dist/css'));
});

// Copy over index files (don't do anything)
gulp.task('copy', function() {
	return gulp.src('src/index.html')
		.pipe(gulp.dest('dist'));
});

gulp.task('copyjson', function() {
	return gulp.src('src/js/config/*.json')
		.pipe(gulp.dest('dist/config'))
});

// Start a Development Local Server for Testing
gulp.task('production', function() {
	browserSync.init({
		server: {
			baseDir: './dist/'
		}
	});

	gulp.watch('src/**/*.*', ['build']);
	gulp.watch('dist/*.html').on('change', browserSync.reload);
});

// Set the Default task (this runs when just running 'gulp')
gulp.task('default', ['production']);

// A build task that only builds production files no local server
gulp.task('build', ['browserify', 'sass', 'copy', 'copyjson']);