var $ 			= require('jquery');
var Backbone 	= require('backbone');
var chapters 	= require('../collections/chapters.collection');
var template 	= require('../templates/navigation.template.hbs');

Backbone.$ = $;

window.allChapters = new chapters();

var NavigationView = Backbone.View.extend({
	el: '#navigation',
	template: template,

	initialize: function() {
		this.collection = new chapters();
		this.collection.bind("reset", this.render, this);
		this.collection.fetch();
	},

	render: function() {
		console.log(this.collection);
		this.$el.html(template({
			chapters: this.collection,
		}));
	}
});

module.exports = NavigationView;