var $ = require('jquery');
var Backbone = require('backbone');
var template = require('../templates/main.template.hbs');

Backbone.$ = $;

var MainView = Backbone.View.extend({
	el: '#main',
	template: template,

	render: function() {
		this.$el.html(template({
			name: 'World',
		}));
	}
});

module.exports = MainView;