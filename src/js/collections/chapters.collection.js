var Backbone = require('backbone');
var chapter = require('../models/chapter.model');

var Chapters = Backbone.Collection.extend({
	defaults: {
		model: chapter
	},
	model: chapter,
	//url: 'http://jsonplaceholder.typicode.com/posts',
	url: '../config/visual_data.json',
	parse: function(response, xhr) {
		return response;
	}
});

module.exports = Chapters;