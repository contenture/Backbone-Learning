var Backbone = require('backbone');
var MainView = require('./views/main.view');
var NavigationView = require('./views/navigation.view');

var Router = Backbone.Router.extend({
	routes: {
		'*path': 'default'
	},

	initialize: function() {
		Backbone.history.start();
	},

	default: function() {
		var view = new MainView();
		var nav = new NavigationView();
		
		view.render();
		nav.render();
	}
});

module.exports = Router;