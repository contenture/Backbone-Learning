# Backbone-Boilerplate
A small boilerplate created internally at RedactiePartners. It servers as a base for our Backbone Projects

# Instructions

`npm install`

`gulp` or `gulp build`

# Includes

- [x] Gulp
- [x] Backbone
- [x] Sample code